import textblob
from textblob import TextBlob
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')

def list_of_words(tweets):
    nltk.download(stopwords)
    stop_words=set(stopwords.words('english'))
    present_words=[]
    for tweet in tweets:
        present_words+=tweet.words
    words_without_stop_words=[word for word in present_words if word not in stop_words]
    return(words_without_stop_words) #renvoie tous les mots qui sont présents dans les tweets et ne sont pas des stop words

def list_of_unique_words(tweets):
    list_of_unique_words=list_of_words(tweets)
    for i in range(len(list_of_words(tweets))): #
        if i>len(list_of_unique_words): #la liste list_of_unique_words diminue de taille petit à petit et donc devient eventuelement plus petite que la liste de mot initiale, i peut donc devenir trop grand (index out of range) ici, on l'evite avec un break
            break
        while list_of_unique_words[i] in list_of_unique_words[0:i]: #on supprime tous les doublons
            list_of_unique_words.pop(i)
    return(list_of_unique_words)


def list_of_words_lemmatized(tweets):
    l=list_of_unique_words(tweets)
    words_lemmatized=[]
    for elem in l:
        words_lemmatized.append(elem.lemmatize)
    return (words_lemmatized) #retourne la liste de mots uniques, lématisée


#test
#from twitter_collect import fonction_deux_etape_deux as fded
#tweets=fded.collect_by_user(1976143068)
#print(tweets)
#print(list_of_words_lemmatized(tweets))
