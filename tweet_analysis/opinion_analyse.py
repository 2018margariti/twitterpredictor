import textblob
from textblob import TextBlob

def opinion_analysis(data):
    pos_tweets=[]
    neu_tweets=[]
    neg_tweets=[]
    for i in range(len(data)): #pour chaque tweet on regarde s'il est plutot positif, négatif ou neutre et on l'ajoute à la liste correspondante
        if data[i,'tweet_textual_content'].sentiment.polarity<-0.3:
            neg_tweets.append(data[i,'tweet_textual_content'])
        if data[i,'tweet_textual_content'].sentiment.polarity>0.3:
            pos_tweets.append(data[i,'tweet_textual_content'])
        else:
            neu_tweets.append(data[i,'tweet_textual_content'])
    print("Percentage of positive tweets: {}%".format(len(pos_tweets)*100/len(data['tweet_textual_content'])))
    print("Percentage of neutral tweets: {}%".format(len(neu_tweets)*100/len(data['tweet_textual_content'])))
    print("Percentage de negative tweets: {}%".format(len(neg_tweets)*100/len(data['tweet_textual_content'])))

