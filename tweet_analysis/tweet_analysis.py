import pandas as pd
import numpy as np
from twitter_collect import fonction_trois_etape_un as fteu

def max_retweets(data):
    rt_max  = np.max(data['RTs'])
    rt  = data[data.RTs == rt_max].index[0]
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))

def longer_tweet(data):
    len_max=np.max(data['len'])
    tweet=data[data.len==len_max].indew[0]
    print("The longer tweet is: \n{}".format(data['tweet_textual_content'][tweet]))
    print("It has {} characters\n".format(len_max))

def max_likes(data):
    likes_max  = np.max(data['likes'])
    likes  = data[data.RTs == likes_max].index[0]
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][likes]))
    print("Number of retweets: {}".format(likes_max))
    print("{} characters.\n".format(data['len'][likes]))

def more_recent_tweet(data):
    date_more_recent=np.max(data['date'])
    more_recent_tweet = data[data.RTs == date_more_recent].index[0]
    print("The tweet the more recent is: \n{}".format(data['tweet_textual_content'][more_recent_tweet]))
    print("It has been written on: {}".format(date_more_recent))

def contain_most_keywords_of_a_candidate(num_candidate,data):
    queries_keywords=fteu.get_candidate_queries(num_candidate,'../CandidateData/keywords_candidate')
    queries_hashtag=fteu.get_candidate_queries(num_candidate,'../CandidateData/hashtag_candidate')
    tweet_with_most_keywords='not_found_yet'
    number_max_keywords=0
    for i in range(len(data)):
        number_of_keywords=0
        for word in data.at[i,'twee_textual_content']:
            if word in queries_keywords or word in queries_hashtag:
                number_of_keywords+=1
            if number_of_keywords>number_max_keywords:
                tweet_with_most_keywords=data.at[i,'tweet_textual_content']
            if number_of_keywords==number_max_keywords:
                tweet_with_most_keywords=tweet_with_most_keywords+' and '+data.at[i,'twee_textual_content']
    if tweet_with_most_keywords=='not_found_yet': #aucun mot clé n'a été trouvé dans les tweets=ne concernent pas le candidat
        print('The tweets do not concern {}'.format(num_candidate))
    else:
        print('the tweet which contains the most keywords about your candidate is : \n{}'.format(tweet_with_most_keywords))
        print ('it contains {} keywords'.format(number_max_keywords))


