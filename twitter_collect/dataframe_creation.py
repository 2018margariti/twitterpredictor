import pandas as pd

def transform_to_dataframe(tweets):
    list_id=[]
    list_text=[]
    list_user=[]
    list_date=[]
    list_likes=[]
    for tweet in tweets :
        list_id.append(tweet.id_str)
        list_text.append(tweet.text)
        list_user.append(tweet.user.id)
        list_date.append(tweet.created_at)
        list_likes.append(tweet.user.favourites_count)
    return(pd.DataFrame({'tweet_id':list_id,'text':list_text,'user_id':list_user,'date':list_date,'likes':list_likes}))

#test
from twitter_collect import fonction_deux_etape_deux as fded
tweets=fded.collect_by_user('@EmmanuelMacron')
print(tweets)
df = transform_to_dataframe(tweets) #df est la dataframe
print(df.head()) #affiche les 5 premiers tweets de la dataframe
print(df.iloc[0,:]) #affiche la 1ère ligne de la dataframe
