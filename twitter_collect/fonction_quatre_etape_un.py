import json

def store_tweets(tweets,file_name):
    with open(file_name+".json", "w") as write_file:
        for tweet in tweets:
            json.dump(tweet, write_file)

store_tweets('bonjour','bonjour') #ce test créé bien un fichier bonjour.json contenant la donnée 'bonjour'

#si on ne veut garder que ce qui est utile du tweet:
def store_tweets_donnees_minimums(tweets,file_name):
    with open(file_name+".json", "w") as write_file:
        for tweet in tweets:
            json.dump(tweet.text, write_file)
            json.dump(tweet.id, write_file)
            json.dump(tweet.hashtag, write_file)
            json.dump(tweet.date, write_file)


#from twitter_collect import collect_tweet_candidate_activity as tcta
#store_tweets(tcta.get_retweets_of_candidate('@Emmanuelmacron'),'tweet_Emmanuel_Macron')

#pour un nommage automatique
def store_tweets_nommage_intelligent(tweets): #le nom du fichier n'est plus un argument, il est determiné par l'algorithme
    d={} #on va compter l'occurence de chaque mots dans les tweets
    for tweet in tweets:
        for word in tweet.text: #on cherche le mot qui apparait le plus dans la collection de tweets
            if word in d:
                d[word]+=1 #le mot est deja dans le dictionnaire de comptage
            else:
                d[word]=1 #premiere occurence du mot
    filename='pas_encore_trouve'
    occurence=0
    for key in d.keys(): #on cherche l'occurence maximale pour definir le nom du fichier
        if d[key]>occurence:
            occurence=d[key]
            filename=key
        if d[key]==occurence: #peut être utile par exemple si on a dans tous les tweets nom et prenom d'un candidat
            filename+=' '+key
    with open(filename+".json", "w") as write_file:
        json.dump(tweets, write_file)
