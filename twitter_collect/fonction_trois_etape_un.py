from twitter_collect import twitter_connection_setup as tcs

def get_candidate_queries(num_candidate, file_path):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    try:
        liste_requetes=[] #création de la liste de requetes
        with open(file_path+'_'+num_candidate+'.txt','r') as f: #ouverture du fichier à parcourir
            for line in f:
                line=line.replace('\n','') #pour chaque ligne on supprime le line break
                liste_requetes.append(line[2::]) #on saute la numérotation
            return(liste_requetes)
    except IOError: #en cas d'erreur rencontrée : fichier non existant
        print("il y a une erreur")



print(get_candidate_queries('num_candidate', '../CandidateData/keywords_candidate')) #on execute pour les mots clés
print(get_candidate_queries('num_candidate', '../CandidateData/hashtag_candidate')) #on execute pour les hashtags

