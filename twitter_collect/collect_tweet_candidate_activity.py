from twitter_collect import twitter_connection_setup as tcs



def get_retweets_of_candidate(num_candidate):
    connexion = tcs.twitter_setup()
    statuses = connexion.user_timeline(id = num_candidate, count = 20) #on collecte les 20 tweets les plus récents du candidat
    for status in statuses: #on parcourt ces 20 tweets
        print(status) #on affiche les informations du tweet
        print(connexion.retweets(id=status.id,count=20)) #pour avoir les 20 derniers retweets de ce tweet.

print(get_retweets_of_candidate('@EmmanuelMacron')) #test

def get_replies_to_candidate(num_candidate):
    connexion = tcs.twitter_setup()
    statuses = connexion.user_timeline(id = num_candidate, count = 20)
    for status in statuses: #on parcourt ces 20 tweets
        print(status) #on affiche les informations du tweet
        reponses=connexion.search(num_candidate,id_replied_to_status=status.id,count=20) #on cherche les tweets qui sont une réponse au tweet considéré
        for reponse in reponses:
            print(reponse)

print(get_replies_to_candidate('@EmmanuelMacron')) #test
