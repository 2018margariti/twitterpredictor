import tweepy
from twitter_collect import twitter_connection_setup as tcs
from twitter_collect import collect_tweet_candidate_activity as ctca
from twitter_collect import tweet_collect_whole as tcw
from twitter_collect import fonction_trois_etape_un as fteu
from twitter_collect import collect_candidate_actuality_tweet as ccat

def collecte(num_candidate):
    connexion=tcs.twitter_setup() #on se connecte
    ctca.get_retweets_of_candidate(num_candidate) #pour les tweets du candidat on regarde tous les retweets
    ctca.get_replies_to_candidate(num_candidate) #et toutes les réponses
    queries=fteu.get_candidate_queries(num_candidate,'../CandidateData/hashtag_candidate') #à partir des mots clefs fournis on créé la liste des requetes
    queries+=fteu.get_candidate_queries(num_candidate,'../CandidateData/keywords_candidate')#idem à partir des hashtags
    ccat.get_tweets_from_candidates_search_queries(queries,connexion) #on recherche les tweets répondant à toutes les requêtes prédefinies
    tcw.collect_by_streaming(num_candidate) #collecte par streaming




