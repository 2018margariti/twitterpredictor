from twitter_collect import twitter_connection_setup as tcs

def collect_by_user(user_id):
    connexion = tcs.twitter_setup()
    statuses = connexion.user_timeline(id = user_id, count = 20)
    for status in statuses:
        print(status.text)
    return statuses

#test
print(collect_by_user(1976143068))
