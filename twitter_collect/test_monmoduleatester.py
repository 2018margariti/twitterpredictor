from twitter_collect import dataframe_creation as dc
from twitter_collect import fonction_deux_etape_un as fdeu
from pytest import *


def test_collect():
    tweets = fdeu.collect()
    data =  dc.transform_to_dataframe(tweets)
    assert 'tweet_textual_content' in data.columns
